
package controlador.controller;

import controlador.Adaptador.AdaptadorDao;
import controlador.tda.lista.ListaEnlazada;
import modelo.User.Despachador;

/**
 *
 * @author Leonardo Peralta
 */
public class DespachadorController extends AdaptadorDao<Despachador> {
    
    private Despachador vend;
    private ListaEnlazada<Despachador> listaVends;
    
    public DespachadorController(){
        super(Despachador.class);
        listado();
    }
    
    public ListaEnlazada<Despachador> getListaVends(){
        return listaVends;
    }
    public void setListaDespachadores(ListaEnlazada<Despachador> listaVends){
        this.listaVends = listaVends;
    }
    
    public Despachador getDespachador(){
        if(this.vend == null){
            this.vend = new Despachador();
        }
        return vend;
    }
    public void setDespachador(Despachador vend){
        this.vend= vend;
    }
    
    public Boolean guardar(){
        try{
            getDespachador().setId(listaVends.getSize()+1);
            guardar(getDespachador());
            return true;
        }catch(Exception e ){
            System.out.println("Error en guardar admin"+e);
        }
        return false;
    }
    public Boolean modificar(Integer pos){
        try{
            modificar(getDespachador());
            return true;
        }catch(Exception e){
            System.out.println("Error en modificar admin"+e);
        }
        return false;
    }
    
    public ListaEnlazada<Despachador> listado(){
            setListaDespachadores(listar());
            return listaVends;
    }
}
