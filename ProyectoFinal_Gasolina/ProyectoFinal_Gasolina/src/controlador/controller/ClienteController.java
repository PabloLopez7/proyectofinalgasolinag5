
package controlador.controller;

import controlador.Adaptador.AdaptadorDao;
import controlador.tda.lista.ListaEnlazada;
import modelo.Cliente;

/**
 *
 * @author Usuario
 */
public class ClienteController extends AdaptadorDao <Cliente> {
    
    private Cliente client;
    private ListaEnlazada<Cliente> listaCliente;
    
    public ClienteController(){
        super(Cliente.class);
        listado();
    }
    
    public ListaEnlazada<Cliente> getListaCliente(){
        return listaCliente;
    }
    public void setListaCliente(ListaEnlazada<Cliente> listaCliente){
        this.listaCliente = listaCliente;
    }
    
    public Cliente getCliente(){
        if(this.client == null){
            this.client = new Cliente();
        }
        return client;
    }
    public void setCliente(Cliente client){
        this.client = client;
    }
    
    public Boolean guardar(){
        try{
            getCliente().setId(listaCliente.getSize()+1);
            guardar(getCliente());
            return true;
        }catch(Exception e ){
            System.out.println("Error en guardar admin"+e);
        }
        return false;
    }
    public Boolean modificar(Integer pos){
        try{
            modificar(getCliente());
            return true;
        }catch(Exception e){
            System.out.println("Error en modificar admin"+e);
        }
        return false;
    }
    
    public ListaEnlazada<Cliente> listado(){
            setListaCliente(listar());
            return listaCliente;
    }
}
