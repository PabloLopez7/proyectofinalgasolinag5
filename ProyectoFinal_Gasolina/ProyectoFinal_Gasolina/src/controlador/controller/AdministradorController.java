package controlador.controller;

import controlador.Adaptador.AdaptadorDao;
import controlador.tda.lista.ListaEnlazada;
import modelo.User.Administrador;


public class AdministradorController extends AdaptadorDao<Administrador> {
    
    private Administrador admin;
    private ListaEnlazada<Administrador> listaAdmin;
    
    public AdministradorController(){
        super(Administrador.class);
        listado();
    }
    
    public ListaEnlazada<Administrador> getListaAdmins(){
        return listaAdmin;
    }
    public void setListaAdmins(ListaEnlazada<Administrador> listaAdmin){
        this.listaAdmin = listaAdmin;
    }
    
    public Administrador getAdministrador(){
        if(this.admin == null){
            this.admin = new Administrador();
        }
        return admin;
    }
    public void setAdministrador(Administrador admin){
        this.admin= admin;
    }
    
    public Boolean guardar(){
        try{
            getAdministrador().setId(listaAdmin.getSize()+1);
            guardar(getAdministrador());
            return true;
        }catch(Exception e ){
            System.out.println("Error en guardar admin"+e);
        }
        return false;
    }
    public Boolean modificar(Integer pos){
        try{
            modificar(getAdministrador());
            return true;
        }catch(Exception e){
            System.out.println("Error en modificar admin"+e);
        }
        return false;
    }
    
    public ListaEnlazada<Administrador> listado(){
            setListaAdmins(listar());
            return listaAdmin;
    }
}
