
package controlador.controller;

import controlador.Adaptador.AdaptadorDao;
import controlador.tda.lista.ListaEnlazada;
import modelo.Persona;

/**
 *
 * @author Usuario
 */
public class PersonaController extends AdaptadorDao<Persona> {
    private Persona p;
    private ListaEnlazada<Persona> listaP;
    
    public PersonaController(){
        super(Persona.class);
        listado();
    }
    
    public ListaEnlazada<Persona> getListaPersona(){
        return listaP;
    }
    public void setListaPersona(ListaEnlazada<Persona> listaP){
        this.listaP = listaP;
    }
    
    public Persona getPersona(){
        if(this.p == null){
            this.p = new Persona();
        }
        return p;
    }
    public void setPersona(Persona p){
        this.p= p;
    }
    
    public Boolean guardar(){
        try{
            getPersona().setId(listaP.getSize()+1);
            guardar(getPersona());
            return true;
        }catch(Exception e ){
            System.out.println("Error en guardar admin"+e);
        }
        return false;
    }
    public Boolean modificar(Integer pos){
        try{
            modificar(getPersona());
            return true;
        }catch(Exception e){
            System.out.println("Error en modificar admin"+e);
        }
        return false;
    }
    
    public ListaEnlazada<Persona> listado(){
            setListaPersona(listar());
            return listaP;
    }
}
