/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import modelo.Cliente;
import modelo.Persona;

/**
 *
 * @author Usuario
 */
public class ClienteDao extends AdaptadorDao<Cliente>{

    private Cliente cliente;
   
    public ClienteDao (){
        super(Cliente.class);
    }

    public Cliente getCliente() {
        if(cliente == null)
            cliente = new Cliente();
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
