/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import modelo.Detalle_Ventas;
import modelo.Persona;

/**
 *
 * @author Usuario
 */
public class DetalleVentaDao extends AdaptadorDao<Detalle_Ventas>{

   private Detalle_Ventas detallesVentas;
   
    public DetalleVentaDao (){
        super(Detalle_Ventas.class);
    }

    public Detalle_Ventas getDetallesVentas() {
        return detallesVentas;
    }

    public void setDetallesVentas(Detalle_Ventas detallesVentas) {
        this.detallesVentas = detallesVentas;
    } 
}
