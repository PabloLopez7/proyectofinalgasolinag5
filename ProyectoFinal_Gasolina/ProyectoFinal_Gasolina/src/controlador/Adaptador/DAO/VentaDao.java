/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import modelo.Ventas;

/**
 *
 * @author Usuario
 */
public class VentaDao extends AdaptadorDao<Ventas>{

   private Ventas venta;
   
    public VentaDao (){
        super(Ventas.class);
    }

    public Ventas getVentas() {
        if(venta == null)
            venta = new Ventas();
        return venta;
    }

    public void setCliente(Ventas venta) {
        this.venta = venta;
    }
    
}
