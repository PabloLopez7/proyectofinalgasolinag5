/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import vista.Producto;

/**
 *
 * @author Usuario
 */
public class ProductoDao extends AdaptadorDao<Producto>{

   private Producto producto;
   
    public ProductoDao (){
        super(Producto.class);
    }

    public Producto getProducto() {
        if(producto == null)
            producto = new Producto();
        return producto;
    }

    public void setCliente(Producto producto) {
        this.producto = producto;
    }
}
