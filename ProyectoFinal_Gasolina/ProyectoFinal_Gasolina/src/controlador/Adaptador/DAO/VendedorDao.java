/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador.Adaptador.DAO;

import controlador.Adaptador.AdaptadorDao;
import modelo.Vendedor;

/**
 *
 * @author Usuario
 */
public class VendedorDao  extends AdaptadorDao<Vendedor>{
    private Vendedor vendedor;
   
    public VendedorDao (){
        super(Vendedor.class);
    }

    public Vendedor getVendedor() {
        if(vendedor == null)
            vendedor = new Vendedor();
        return vendedor;
    }

    public void setCliente(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
}
