/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.servicios;


import controlador.controller.PersonaController;
import controlador.tda.lista.ListaEnlazada;

import modelo.Persona;

/**
 * @author Leonardo Peralta
 */
public class ServicioPersona {
    
    private PersonaController pc = new PersonaController();
    
     
    public Persona getPersona(){
        return pc.getPersona();
    }
    public ListaEnlazada<Persona>getListaArchivo(){
        return pc.listado();
    }
    public Boolean guardar(){
        return pc.guardar();
    }
    public Boolean modificar(Integer pos){
        return pc.modificar(pos);
    }
    public void setPersona(Persona p ){
        pc.setPersona(p);
    }
    
}
