
package controlador.servicios;

import controlador.controller.DespachadorController;
import controlador.tda.lista.ListaEnlazada;
import modelo.User.Despachador;

/**
 *
 * @author Usuario
 */
public class ServicioDespachador {
    private DespachadorController dpc = new DespachadorController();
    
    public Despachador getDespach(){
        return dpc.getDespachador();
    }
    public ListaEnlazada<Despachador>getListaArchivo(){
        return dpc.listado();
    }
    public Boolean guardar(){
        return dpc.guardar();
    }
    public Boolean modificar(Integer pos){
        return dpc.modificar(pos);
    }
    public void setDespach(Despachador vend){
        dpc.setDespachador(vend);
    }
}
