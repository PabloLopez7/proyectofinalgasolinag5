
package controlador.servicios;

import controlador.controller.AdministradorController;
import controlador.tda.lista.ListaEnlazada;
import modelo.User.Administrador;

/**
 *
 * @author Leonardo Peralta
 */
public class ServicioAdministrador {
    private AdministradorController adc = new AdministradorController();
    
    public Administrador getAdmin(){
        return adc.getAdministrador();
    }
    public ListaEnlazada<Administrador>getListaArchivo(){
        return adc.listado();
    }
    public Boolean guardar(){
        return adc.guardar();
    }
    public Boolean modificar(Integer pos){
        return adc.modificar(pos);
    }
    public void setAdmin(Administrador admin){
        adc.setAdministrador(admin);
    }
}
