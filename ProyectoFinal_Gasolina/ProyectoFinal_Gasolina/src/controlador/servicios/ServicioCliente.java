
package controlador.servicios;

import controlador.controller.ClienteController;
import controlador.tda.lista.ListaEnlazada;
import modelo.Cliente;

public class ServicioCliente {
    
    private ClienteController clc = new ClienteController();
    
    public Cliente getCliente(){
        return clc.getCliente();
    }
    public ListaEnlazada<Cliente>getListaArchivo(){
        return clc.listado();
    }
    public Boolean guardar(){
        return clc.guardar();
    }
    public Boolean modificar(Integer pos){
        return clc.modificar(pos);
    }
    public void setCliente(Cliente client){
        clc.setCliente(client);
    }
}
