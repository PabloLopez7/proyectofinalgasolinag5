
package modelo.enums;

/**
 *
 * @author Leonardo Peralta
 */
public enum TipoIdentificacion {
    CEDULA, PASAPORTE;
}
