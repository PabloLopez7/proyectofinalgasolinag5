
package modelo.enums;

public enum TipoCombustible {
    ECOPAIS, SUPER, DIESEL;
}
