/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

import java.util.Date;

/**
 *
 * @author Usuario
 */
public class Ventas {
    private int id;
    private Cliente cliente;
    private Producto producto;
    private int nroVentas;
    private Date fechaVentas;
    private Double monto;

    public Ventas() {
    }

    public Ventas(int id, Cliente cliente, Producto producto, int nroVentas, Date fechaVentas, Double monto) {
        this.id = id;
        this.cliente = cliente;
        this.producto = producto;
        this.nroVentas = nroVentas;
        this.fechaVentas = fechaVentas;
        this.monto = monto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public int getNroVentas() {
        return nroVentas;
    }

    public void setNroVentas(int nroVentas) {
        this.nroVentas = nroVentas;
    }

    public Date getFechaVentas() {
        return fechaVentas;
    }

    public void setFechaVentas(Date fechaVentas) {
        this.fechaVentas = fechaVentas;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }
    
    
    
}
