
package modelo;

import modelo.enums.EstadoEmpleado;
import modelo.enums.TipoEmpleado;
import modelo.enums.TipoIdentificacion;

public class Persona {
    private Integer id;
    private String nombre;
    private String apellido;
    private TipoIdentificacion tipo_identificacion;
    private EstadoEmpleado estadoEmpleado;
    private TipoEmpleado tipoempleado;
    private String Domicilio;
    private String contacto;
    private String Usuario;
    private String Contra;
    private String Identificacion;
    
    
    
    public String getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(String Identificacion) {
        this.Identificacion = Identificacion;
    }
   
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public TipoIdentificacion getTipo_identificacion() {
        return tipo_identificacion;
    }

    public void setTipo_identificacion(TipoIdentificacion tipo_identificacion) {
        this.tipo_identificacion = tipo_identificacion;
    }

    public EstadoEmpleado getEstadoEmpleado() {
        return estadoEmpleado;
    }

    public void setEstadoEmpleado(EstadoEmpleado estadoEmpleado) {
        this.estadoEmpleado = estadoEmpleado;
    }

    public TipoEmpleado getTipoempleado() {
        return tipoempleado;
    }

    public void setTipoempleado(TipoEmpleado tipoempleado) {
        this.tipoempleado = tipoempleado;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String Domicilio) {
        this.Domicilio = Domicilio;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getContra() {
        return Contra;
    }

    public void setContra(String Contra) {
        this.Contra = Contra;
    }
}
