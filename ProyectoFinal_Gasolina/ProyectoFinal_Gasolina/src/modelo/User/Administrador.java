
package modelo.User;

import modelo.Persona;
import modelo.enums.TipoEmpleado;
import modelo.enums.TipoIdentificacion;

/**
 *
 * @author Leonardo Peralta
 */
public class Administrador extends Persona  {
    private String Usuario="admin1104";
    private String Contra="admincontra";
    private String nombre;
    private String apellido;
    private Integer id;
    private String contacto;
    private TipoIdentificacion tipoIdent;
    private TipoEmpleado tipoempleado;
    private String Domicilio;
    
    @Override
    public String getUsuario() {
        return Usuario;
    }
    @Override
    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }
    
    @Override
    public String getContra() {
        return Contra;
    }
    
    @Override
    public void setContra(String Contra) {
        this.Contra = Contra;
    }
    
    @Override
    public String getNombre() {
        return nombre;
    }

    @Override
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    @Override
    public String getApellido() {
        return apellido;
    }
    @Override
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    @Override
    public Integer getId() {
        return id;
    }
    @Override
    public void setId(Integer id) {
        this.id = id;
    }
    @Override
    public String getContacto() {
        return contacto;
    }
    @Override
    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public TipoIdentificacion getTipoIdent() {
        return tipoIdent;
    }

    public void setTipoIdent(TipoIdentificacion tipoIdent) {
        this.tipoIdent = tipoIdent;
    }
    @Override
    public TipoEmpleado getTipoempleado() {
        return tipoempleado;
    }
    @Override
    public void setTipoempleado(TipoEmpleado tipoempleado) {
        this.tipoempleado = tipoempleado;
    }
    @Override
    public String getDomicilio() {
        return Domicilio;
    }
    @Override
    public void setDomicilio(String Domicilio) {
        this.Domicilio = Domicilio;
    }

}
