/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo;

/**
 *
 * @author Usuario
 */
public class Detalle_Ventas {
    private int id;
    private Ventas ventas;
    private Producto producto;
    private int cantidad;
    private int precioVentas;

    public Detalle_Ventas() {
    }

    public Detalle_Ventas(int id, Ventas ventas, Producto producto, int cantidad, int precioVentas) {
        this.id = id;
        this.ventas = ventas;
        this.producto = producto;
        this.cantidad = cantidad;
        this.precioVentas = precioVentas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Ventas getVentas() {
        return ventas;
    }

    public void setVentas(Ventas ventas) {
        this.ventas = ventas;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getPrecioVentas() {
        return precioVentas;
    }

    public void setPrecioVentas(int precioVentas) {
        this.precioVentas = precioVentas;
    }
    
    
    
    
}
