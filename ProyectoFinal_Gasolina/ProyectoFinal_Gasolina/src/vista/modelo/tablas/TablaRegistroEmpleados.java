
package vista.modelo.tablas;

import javax.swing.table.AbstractTableModel;
import modelo.Persona;

/**
 *
 * @author Leonardo Peralta
 */
public class TablaRegistroEmpleados extends AbstractTableModel{
    
    private Persona person;

    @Override
    public int getRowCount() {
        return 5;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); 
    }
   
      
}
