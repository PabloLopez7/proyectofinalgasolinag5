/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package vista;

import controlador.Conexion.Conexion;
import controlador.utilidades.Utilidades;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Usuario
 */
public class RegistrarCliente extends javax.swing.JFrame {

    Conexion conn = new Conexion();
    Connection conet;
    DefaultTableModel modelo;
    Statement st;
    ResultSet rs;
    int idc;
    

    /**
     * Creates new form RegistrarCliente1
     */
    public RegistrarCliente() {
        initComponents();
        setLocationRelativeTo(null);
        this.setLocationRelativeTo(this);
        consultar();
        cargarTipo();
    }

    private void cargarTipo() {
        cbxtipo.removeAllItems();
        for (String aux : Utilidades.tipoIdentificacion()) {
            cbxtipo.addItem(aux);
        }
    }

    void consultar() {
        String sql = "SELECT * FROM `cliente`";
        try {
            conet = conn.getConnection();
            st = conet.createStatement();
            rs = st.executeQuery(sql);
            Object[] cliente = new Object[6];
            modelo = (DefaultTableModel) Tabla.getModel();
            while (rs.next()) {
                cliente[0] = rs.getInt("id");
                cliente[1] = rs.getInt("Cedula");
                cliente[2] = rs.getString("Apellido");
                cliente[3] = rs.getString("Nombre");
                cliente[4] = rs.getString("Direccion");
                cliente[5] = rs.getInt("Telefono");

                modelo.addRow(cliente);
            }
            Tabla.setModel(modelo);
        } catch (Exception e) {

        }
    }

    void Agregar() {
        String id = txtId.getText();
        String cedu = txtCedula.getText();
        String ape = txtApellido.getText();
        String nom = txtNombre.getText();
        String dir = txtDireccion.getText();
        String tele = txtTelefono.getText();

        try {
            if (id.equals("") || cedu.equals("") || ape.equals("") || nom.equals("") || dir.equals("") || tele.equals("")) {// nos muetsra si las caja de texto estan vacios
                JOptionPane.showMessageDialog(null, "Faltan Ingresar Datos");
                limpiarTabla();
            } else {
                String sql = "INSERT INTO `cliente`(id, Cedula, Apellido, Nombre, Direccion, Telefono) VALUES ('" + id + "', '" + cedu + "','" + ape + "','" + nom + "','" + dir + "','" + tele + "')";
                conet = conn.getConnection();
                st = conet.createStatement();
                st.executeUpdate(sql);
                JOptionPane.showMessageDialog(null, "Nuevo Cliente Registrado");
                limpiarTabla();
            }
        } catch (Exception e) {
        }
    }

    void limpiarTabla() {
        for (int i = 0; i <= Tabla.getRowCount(); i++) {
            modelo.removeRow(i);
            i = i - 1;
        }
    }

    void modificar() {
        String id = txtId.getText();
        String cedu = txtCedula.getText();
        String ape = txtApellido.getText();
        String nom = txtNombre.getText();
        String dir = txtDireccion.getText();
        String tele = txtTelefono.getText();

        try {
            if (id.equals("") || cedu.equals("") || ape.equals("") || nom.equals("") || dir.equals("") || tele.equals("")) {// nos muetsra si las caja de texto estan vacios
                JOptionPane.showMessageDialog(null, "Faltan Ingresar Datos");
                limpiarTabla();
            } else {
                String sql = "UPDATE `cliente` SET id ='" + idc + "', Cedula='" + cedu + "', Apellido = '" + ape + "', Nombre = '" + nom + "', Direccion = '" + dir + "', Telefono = '" + tele + "' where id =" + idc;
                conet = conn.getConnection();
                st = conet.createStatement();
                st.executeUpdate(sql);
                JOptionPane.showMessageDialog(null, "Datos Cliente Modificados");
                limpiarTabla();
            }
        } catch (Exception e) {
        }
    }

    void eliminar() {
        int fila = Tabla.getSelectedRow();
        try {
            if (fila < 0) {
                JOptionPane.showMessageDialog(null, "Cliente no seleccionado");
                limpiarTabla();
            } else {
                String sql = "DELETE FROM `cliente` WHERE id=" + idc;
                conet = conn.getConnection();
                st = conet.createStatement();
                st.executeUpdate(sql);
                JOptionPane.showMessageDialog(null, "Cliente Eliminado");
                limpiarTabla();
            }
        } catch (Exception e) {
        }
    }

    void Nuevo() {
        txtId.setText("");
        txtCedula.setText("");
        txtApellido.setText("");
        txtNombre.setText("");
        txtDireccion.setText("");
        txtTelefono.setText("");
        txtId.requestFocus();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        cbxtipo = new javax.swing.JComboBox<String>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        txtCedula = new javax.swing.JTextField();
        txtApellido = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        txtTelefono = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        Tabla = new javax.swing.JTable();
        jButton7 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("REGISTRO DE CLIENTES ");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(280, 70, 540, 32);

        jPanel2.setBackground(new java.awt.Color(0, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Datos Clientes")));
        jPanel2.setLayout(null);

        jLabel2.setText("TIPO DE IDENTIFICACION : ");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(20, 40, 200, 14);

        cbxtipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(cbxtipo);
        cbxtipo.setBounds(190, 40, 140, 22);

        jLabel3.setText("ID: ");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(20, 90, 37, 14);

        jLabel4.setText("IDENTIFICACION : ");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(20, 140, 150, 14);

        jLabel5.setText("APELLIDO: ");
        jPanel2.add(jLabel5);
        jLabel5.setBounds(20, 190, 100, 14);

        jLabel6.setText("NOMBRE: ");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(20, 240, 90, 14);

        jLabel7.setText("DIRECCION: ");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(20, 290, 100, 14);

        jLabel8.setText("TELEFONO");
        jPanel2.add(jLabel8);
        jLabel8.setBounds(20, 340, 90, 14);
        jPanel2.add(txtId);
        txtId.setBounds(160, 82, 170, 30);
        jPanel2.add(txtCedula);
        txtCedula.setBounds(160, 130, 170, 30);
        jPanel2.add(txtApellido);
        txtApellido.setBounds(160, 180, 170, 30);
        jPanel2.add(txtNombre);
        txtNombre.setBounds(160, 230, 170, 30);
        jPanel2.add(txtDireccion);
        txtDireccion.setBounds(160, 280, 170, 30);
        jPanel2.add(txtTelefono);
        txtTelefono.setBounds(160, 330, 170, 30);

        jButton1.setText("AGREGAR");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1);
        jButton1.setBounds(20, 380, 100, 23);

        jButton2.setText("CANCELAR");
        jPanel2.add(jButton2);
        jButton2.setBounds(230, 380, 110, 23);

        jButton5.setText("NUEVO");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton5);
        jButton5.setBounds(130, 380, 75, 23);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(20, 110, 350, 420);

        jPanel3.setBackground(new java.awt.Color(0, 153, 153));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "Registro Clientes"));
        jPanel3.setLayout(null);

        jButton3.setText("MODIFICAR");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton3);
        jButton3.setBounds(25, 380, 100, 23);

        jButton4.setText("ELIMINAR");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton4);
        jButton4.setBounds(150, 380, 100, 23);

        jButton6.setText("ATRAS");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton6);
        jButton6.setBounds(420, 380, 67, 23);

        Tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "IDENTIFICACION", "APELLIDO", "NOMBRE", "DIRECCION", "TELEFONO"
            }
        ));
        Tabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                TablaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(Tabla);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(30, 50, 480, 290);

        jButton7.setText("Pantalla Principal");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton7);
        jButton7.setBounds(280, 380, 120, 23);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(390, 110, 520, 420);

        jLabel9.setFont(new java.awt.Font("Imprint MT Shadow", 1, 26)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("SISTEMA DE SERVICIO \"LA ARGELIA\" ");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(160, 10, 551, 32);

        jLabel10.setFont(new java.awt.Font("Imprint MT Shadow", 1, 26)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("(GASOLINERA)");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(310, 40, 212, 32);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 934, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 538, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        modificar();
        consultar();
        Nuevo();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        Agregar();
        consultar();
        Nuevo();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        eliminar();
        consultar();
        Nuevo();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        Nuevo();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void TablaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_TablaMouseClicked
        // TODO add your handling code here:
        int fila = Tabla.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(null, "No se selecciono Fila");
        } else {
            idc = Integer.parseInt((String) Tabla.getValueAt(fila, 0).toString());
            int cedu = Integer.parseInt((String) Tabla.getValueAt(fila, 1).toString());
            String ape = (String) Tabla.getValueAt(fila, 2);
            String nom = (String) Tabla.getValueAt(fila, 3);
            String dir = (String) Tabla.getValueAt(fila, 4);
            int tele = Integer.parseInt((String) Tabla.getValueAt(fila, 5).toString());

            txtId.setText("" + idc);
            txtCedula.setText("" + cedu);
            txtApellido.setText(ape);
            txtNombre.setText(nom);
            txtDireccion.setText(dir);
            txtTelefono.setText("" + tele);
        }
    }//GEN-LAST:event_TablaMouseClicked

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        VentanaEmpleado  newframe = new VentanaEmpleado();
        newframe.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        Principal newFrame = new Principal();
       newFrame.setVisible(true);
       this.dispose();
    }//GEN-LAST:event_jButton7ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistrarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistrarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistrarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistrarCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistrarCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Tabla;
    private javax.swing.JComboBox<String> cbxtipo;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtApellido;
    private javax.swing.JTextField txtCedula;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtTelefono;
    // End of variables declaration//GEN-END:variables
}
